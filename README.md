CONTENTS OF THIS FILE
---------------------
 * Introduction
 * FileOverview
 * Requirements
 * Usage  
 * Maintainers


INTRODUCTION
------------

Word Calculator Takes in words as strings and turn them into numbers that can then be +-/* or = again using the words for them "plus", "minus", "divided", "times" and "equals"

Example Code

Plus
"HOW TO DO Number + Number"

nine_thousand_nine_hundred_ninety_nine
plus
fifty_five
equals

Minus
"HOW TO DO Number - Number"

nine_thousand_nine_hundred_ninety_nine
minus
fifty_five
equals

Dividing
"HOW TO DO Number / Number"

nine_thousand_nine_hundred_ninety_nine
divided
fifty_five
equals

Times
"HOW TO DO Number * Number"

nine_thousand_nine_hundred_ninety_nine
times
fifty_five
equals

BUGS
------------
* Cant enter above 9999 reliably (calculations can still be done up to float max)

* Equals can sometimes "work" in suprising ways



FILEOVERVIEW
------------

* main.cpp - Is the mainmenu sort of script that you are always supposed to be returned to

* inputHandler.cpp - Is the almost finished system where all input is supposed to go through. It makes sure the input is valid and NOT case sensetive.

* userCommands.cpp - A container for all the hardcoded commands such as help,start,mainmenu,exit that the inputHandler.cpp can reference in order to recognize different commands

* Commands.cpp - The Container userCommands.cpp references to when a valid command is typed by the user

* wordCalculator.cpp - The script that handles the main logic of the word calculator such as taking in strings and making them into "real" numbers

* calculatorCommands.cpp - The script handeling the logic of the word calculator such as the +-/* and = logic of the numbers found in wordCalculator.cpp
   
REQUIREMENTS
------------

* Standard C++ VisualStudioCode


INSTALLATION
------------

* Install as you would normally install a VisualStudioCode Project

* Simply start up the main file found in the build folder and follow instructions on screen

USAGE
------------

Everything in this is free to use by anyone.

MAINTAINERS
------------

rasmus.braunstein.jensen@dk.experis.com
