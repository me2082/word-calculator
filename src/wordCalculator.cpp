#include "wordCalculator.h"
#include "userCommands.h"
#include "inputHandler.h"
#include "calculatorCommands.h"
#include <map>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> words;

string word;

vector<int> NumbersToPutTogether;

int test;

bool lookingForOperator = false;

// map was too confusing and just dident work
string wordNumbers[30]{{"zero"}, {"one"}, {"two"}, {"three"}, {"four"}, {"five"}, {"six"}, {"seven"}, {"eight"}, {"nine"}, {"ten"}, {"eleven"}, {"twelve"}, {"thirteen"}, {"Fourteen"}, {"fifteen"}, {"sixteen"}, {"seventeen"}, {"Eighteen"}, {"nineteen"}, {"twenty"}, {"thirty"}, {"forty"}, {"fifty"}, {"sixty"}, {"seventy"}, {"eighty"}, {"ninety"}, {"hundred"}, {"thousand"}};

int numbers[30]{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {30}, {40}, {50}, {60}, {70}, {80}, {90}, {100}, {1000}};

void applicationDataHandler(string InputData)
{
    // Makes sure we dont have any old garbage data
    NumbersToPutTogether.clear();
    words.clear();
    word = "";
    test = 0;

    cout << InputData << endl;
    cin.get();
    if (true)
    {

        if (lookingForOperator == true)
        {
            lookingForOperator = false;
            operatorReader(InputData);
        }

        // Gets the needed size for the char array
        int n = InputData.length();

        // Created Char Array
        char charArray[n + 1];

        // Adds the string to the chararray
        strcpy(charArray, InputData.c_str());

        size_t size = sizeof(charArray) / sizeof(charArray[0]);

        for (int i = 0; i < size; i++)
        {
            if (charArray[i] == '_' || i == size - 1)
            {
                for (int j = 0; j < i; j++)
                {
                    word = word + charArray[j];
                }

                for (int l = 0; l < i + 1; l++)
                {
                    charArray[l] = '\0';
                }

                words.push_back(word);

                word = "";
            }
        }
        for (int q = 0; q < words.size(); q++)
        {

            words[q].erase(remove(words[q].begin(), words[q].end(), '\0'), words[q].end());
        }

        for (int y = 0; y < words.size(); y++)
        {
            for (int o = 0; o < sizeof(wordNumbers) / sizeof(wordNumbers[0]); o++)
            {
                if (words[y] == wordNumbers[o])
                {
                    NumbersToPutTogether.push_back(numbers[o]);
                }
            }
        }
        for (int p = 0; p < NumbersToPutTogether.size(); p++)
        {
            if (NumbersToPutTogether[p] < 10 && NumbersToPutTogether[p] > 0 && NumbersToPutTogether[p + 1] == 100 && (p + 1) <= NumbersToPutTogether.size() && NumbersToPutTogether.size() != 1 || NumbersToPutTogether[p] < 100 && NumbersToPutTogether[p + 1] == 1000 && NumbersToPutTogether[p] > 0 && (p + 1) <= NumbersToPutTogether.size() && NumbersToPutTogether.size() != 1)
            {
                test = test + NumbersToPutTogether[p] * NumbersToPutTogether[p + 1];
                p++;
            }
            else
            {

                test = test + NumbersToPutTogether[p];
            }
        }
        cout << "You entered: " << test << endl;

        sendNum(test);
    }
    else
    {
        userCommands(InputData);
    }
}

void wordCalculator()
{
    inputHandler(true, true, "Please enter a number as a word/words with underscore _ inbetween each number in that number. \nPlease note highest supported number is 9999 though it is possible to go higher success may wary  \nExample: nine_thousand_nine_hundred_ninety_nine = 9999");
}

void operatorTime()
{
    lookingForOperator = true;
}
