#include "inputHandler.h"
#include "wordCalculator.h"
#include <iostream>
#include <array>
#include <limits>    //to get the numeric_limits
#include <algorithm> //to use the for_each

string openingMessage = "Word cal \nA Calculator that takes text numbers instead of normal numbers becouse that would be too easy";

string input;

void ClearScreen()
{
    cout << "\033[2J\033[1;1H";
}

void OpeningStatement()
{
    ClearScreen();
    cout << openingMessage << endl;
}

// Awaits valid input & makes it lowercase so its not case sensetive
void inputHandler(bool clearScreen, bool applicationIsRunning, string message)
{

    if (clearScreen == true)
    {
        ClearScreen();
    }

    // Makes sure we get the correct input and actually get an input
    while (cout << message << "\n" && !(cin >> input))
    {
        // Clear bad input flag
        cin.clear();

        // discard input insane input
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        cout << "Invalid input \n Please type (help) for a list of commands...\n";
    }

    // how to make a string all lowercase
    for_each(input.begin(), input.end(), [](char &c)
             { c = ::tolower(c); });

    if (applicationIsRunning == true)
    {
        applicationDataHandler(input);
    }
    else
    {
        // sends the input to the inputchecker
        userCommands(input);
    }
}