#include <iostream>
#include "inputHandler.h"
#include "calculatorCommands.h"
#include "wordCalculator.h"
#include <string>

using namespace std;

float Answer = 0;

float a = 420420420;
float b = 69696969;

string calculationOperator;

bool calculationTrigger = false;

string operators[5]{{"plus"}, {"minus"}, {"times"}, {"divide"}, {"equal"}};

void sendNum(int c)
{
    if (a == 420420420)
    {
        a = c;
        operatorTime();
        inputHandler(false, true, "Please input an operator.\nExample: plus minus times divide equal");
    }
    else if (b == 69696969)
    {
        b = c;
        calculationTrigger = true;
        operatorReader(calculationOperator);
    }
}

void myPlus()
{
    if (b == 69696969)
    {
        Answer = Answer + a;
    }
    else
    {
        Answer = Answer + (a + b);
        calculationTrigger = true;
    }
}
void myMinus()
{
    if (b == 69696969)
    {
        Answer = Answer - a;
    }
    else
    {
        Answer = Answer + (a - b);
        calculationTrigger = true;
    }
}
void myTimes()
{
    if (b == 69696969)
    {
        Answer = Answer * a;
    }
    else
    {
        Answer = Answer + (a * b);
        calculationTrigger = true;
    }
}
void myDivide()
{
    if (b == 69696969)
    {
        Answer = Answer / a;
    }
    else
    {
        Answer = Answer + (a / b);
        calculationTrigger = true;
    }
}
void myEquals()
{
    if (calculationTrigger == true)
    {
        cout << Answer << endl;
        cout << "Press Any Key to Continue..." << endl;
        cin.get();
        inputHandler(false);
    }
    else if (a != 420420420)
    {
        cout << a << endl;
        cout << "Press Any Key to Continue..." << endl;
        cin.get();
        inputHandler();
    }
}

void operatorReader(string operatorInput)
{
    if (operatorInput == operators[0])
    {
        if (calculationTrigger == true)
        {
            myPlus();
            cout << "Current Answer: " << Answer << endl;
            resetAB();
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
        else
        {
            calculationOperator = operatorInput;
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
    }
    else if (operatorInput == operators[1])
    {
        if (calculationTrigger == true)
        {
            myMinus();
            cout << "Current Answer: " << Answer << endl;
            resetAB();
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
        else
        {
            calculationOperator = operatorInput;
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
    }
    else if (operatorInput == operators[2])
    {
        if (calculationTrigger == true)
        {
            myTimes();
            cout << "Current Answer: " << Answer << endl;
            resetAB();
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
        else
        {
            calculationOperator = operatorInput;
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
    }
    else if (operatorInput == operators[3])
    {
        if (calculationTrigger == true)
        {
            myDivide();
            cout << "Current Answer: " << Answer << endl;
            resetAB();
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
        else
        {
            calculationOperator = operatorInput;
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
    }
    else if (operatorInput == operators[3])
    {
        if (calculationTrigger == true)
        {
            myEquals();
            cout << "Current Answer: " << Answer << endl;
            resetAB();
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
        else
        {
            calculationOperator = operatorInput;
            inputHandler(false, true, "Please enter another number or type equal to get final result");
        }
    }
    else
    {
        inputHandler();
    }
}

void resetAB()
{
    calculationOperator = "";
    a = 420420420;
    b = 69696969;
}