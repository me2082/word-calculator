#include "userCommands.h"
#include "Commands.h"
#include "inputHandler.h"
#include <iostream>

string commandList[4] = {"help", "start", "menu", "exit"};

int numberoOfElementsInArray = sizeof(commandList) / sizeof(commandList[0]);

// Checks if we have a command equal to what the user put in
void userCommands(string userInput)
{
    if (commandList[0] == userInput)
    {
        // Input is "help"
        helpCommand(commandList, numberoOfElementsInArray);
    }
    else if (commandList[1] == userInput)
    {
        // Input is "start"
        startCommand();
    }
    else if (commandList[2] == userInput)
    {
        // Input is "menu"
        menuCommand();
    }
    else if (commandList[3] == userInput)
    {
        // Input is "exit"
        exitProgram();
    }
    else
    {
        // Invalid input
        ClearScreen();
        // cout << "Invalid input \n Please type (help) for a list of commands...\n";
        inputHandler();
    }
}