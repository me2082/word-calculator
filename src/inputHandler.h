#include <string>
using namespace std;

//Standard Clears screen before new message & takes only commands
void inputHandler(bool clearScreen = true, bool applicationIsRunning = false, string message = "Type (help) for a list of commands..");

void OpeningStatement();

void userCommands(string userInput);

void helpCommand();

void ClearScreen();