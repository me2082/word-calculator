#include "Commands.h"
#include "inputHandler.h"
#include "wordCalculator.h"
#include <iostream>

void helpCommand(string commands[], int length)
{
    ClearScreen();
    cout << "Available Commands are:" << endl;

    int i;
    for (i = 0; i < length; i++)
    {
        cout << commands[i] << endl;
    }
    cout << "\n" << "For more info see documentation." << "\n" << endl;
    inputHandler(false); // TODO change this to some other system when its there
}

void startCommand()
{
    // Allows us to choose what program we are gonna start
    wordCalculator();
}

void menuCommand()
{
    OpeningStatement();
    inputHandler(false);
}

void exitProgram()
{
}